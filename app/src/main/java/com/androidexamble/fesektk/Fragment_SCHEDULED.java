package com.androidexamble.fesektk;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidexamble.fesektk.Adapter_Scheduled.ScheduledcontactsrecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static android.support.constraint.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_SCHEDULED extends Fragment   {
    List<ResponseScheduled>list;

    public Fragment_SCHEDULED() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_recyclerview__sceduled, container, false);

        final RecyclerView ScheduledRecyclerView = view.findViewById(R.id.contact_recycler_view);
        ScheduledRecyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity()));
        SharedPreferences preferences = this.getContext().getSharedPreferences("MYPREFS", MODE_PRIVATE);
        String token = preferences.getString("user_token", null);


        Apimanger.getApis().getScheduled(token).enqueue(new Callback<ResponseScheduled>() {
            @Override
            public void onResponse(Call<ResponseScheduled> call, Response<ResponseScheduled> response) {
                if (response.isSuccessful()) {
                    ResponseScheduled items = response.body();
                    List<scheduleOfferRequest> scheduleOfferRequests = new ArrayList<scheduleOfferRequest>();
                    assert items != null;
                    scheduleOfferRequests.addAll(items.getScheduleOffer());
                    scheduleOfferRequests.addAll(items.getScheduleRequest());

                    ScheduledcontactsrecyclerAdapter ScheduledcontactsrecyclerAdapter = new ScheduledcontactsrecyclerAdapter(getContext(), scheduleOfferRequests);
                    ScheduledRecyclerView.setAdapter(ScheduledcontactsrecyclerAdapter);


                    Log.d(TAG,"AAAAAAAAAAAAAAAA"+ items.toString());


                }


            }

            @Override
            public void onFailure(Call<ResponseScheduled> call, Throwable t) {
                Log.d(TAG,"ffffffffffffffffffffffffff"+ t.getMessage());

            }
        });
        return view;
    }



}