package com.androidexamble.fesektk;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Apimanger {
    private static Retrofit retrofitInstance;

    private static Retrofit getInstance() {
        if (retrofitInstance == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder()
                    .addInterceptor(logging);


            retrofitInstance = new Retrofit.Builder()
                    .baseUrl("http://1a836af8.ngrok.io")
                    .addConverterFactory(GsonConverterFactory.create())
                    .callFactory(httpClientBuilder.build())
                    .build();


        }
        return retrofitInstance;
    }

    public static Apicalls getApis() {
        Apicalls apicalls = getInstance().create(Apicalls.class);
        return apicalls;

    }
}
