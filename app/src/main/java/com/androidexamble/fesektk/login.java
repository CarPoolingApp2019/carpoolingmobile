package com.androidexamble.fesektk;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import static com.androidexamble.fesektk.phonecode.PHONE_NUMBER;


public class login extends AppCompatActivity  {


    private EditText editText;


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        editText = findViewById(R.id.phonenumber);


        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String number = editText.getText().toString().trim();
                if (number.isEmpty() || number.length() !=13) {
                    editText.setError("number is required");
                    editText.requestFocus();
                    return;


                }
                Intent intent = new Intent(login.this,phonecode.class);
                intent.putExtra(PHONE_NUMBER, number);
                startActivity(intent);
            }


        });


    }



}











