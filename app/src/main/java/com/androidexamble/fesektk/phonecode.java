package com.androidexamble.fesektk;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.androidexamble.fesektk.PushNotificationService.MyFirebaseInstanceIdService;
import com.androidexamble.fesektk.PushNotificationService.SendNotificationTokenTask;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.firebase.auth.FirebaseAuth.getInstance;

public class phonecode extends AppCompatActivity {
    private  String varificationid ;
    private FirebaseAuth mAuth;
    private EditText editText;
    private String number;
    public static final String PHONE_NUMBER = "phoneNumber";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phonecode);

        FirebaseApp.initializeApp(this);
        mAuth= getInstance();
        editText=findViewById(R.id.code);
        number = getIntent().getStringExtra(PHONE_NUMBER);
        final String phonenumber = getIntent().getStringExtra(PHONE_NUMBER);
        sendvarificationcode(phonenumber);
        findViewById(R.id.countiue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editText.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    editText.setError("Enter code");
                    editText.requestFocus();
                    return;
                }

                varifycode(code);


            }
        });
    }
    private  void  varifycode(String code){
        //  PhoneAuthCredential credential =PhoneAuthProvider.getCredential(varificationid,code);
        // sininwithcredential(credential);
        Log.d("phoneCode" , "inside verify code");
        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(varificationid, code);
            sininwithcredential(credential);
        } catch (Exception e) {
            Log.d("exception", e.toString());
            Toast.makeText(phonecode.this, "Invalid credentials", Toast.LENGTH_LONG).show();
        }

    }


    private void sininwithcredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    Log.d("testPhone", "" + number);

                    Apimanger.getApis().getLoginUser(number).enqueue(new Callback <UserResponse>() {

                                @Override
                                public void onResponse(Call <UserResponse> call, Response <UserResponse> response) {
                                    if (response.isSuccessful()) {
                                            Log.d("testLog", response.body().toString());
                                            //TODO call sendDeviceToken
                                        String Registeration_token = response.body().getToken();
                                        Log.d("register_token" , "userToken"+Registeration_token);

                                        SharedPreferences preferences = getSharedPreferences("MYPREFS",MODE_PRIVATE);
                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putString("user_token",Registeration_token);
                                        editor.commit();

                                        MyFirebaseInstanceIdService myFirebaseInstanceIdService = new MyFirebaseInstanceIdService();
                                        String notificationToken = myFirebaseInstanceIdService.getDeviceTokens(getApplicationContext());

                                        SendNotificationTokenTask sendNotificationTokenTask = new SendNotificationTokenTask();
                                        sendNotificationTokenTask.sendToken(Registeration_token, notificationToken);

                                        Intent intent = new Intent(phonecode.this, BasicActivity.class);
                                        startActivity(intent);
                                    } else if (response.code() == 403) {
                                        Intent intent = new Intent(phonecode.this, signup.class);
                                        startActivity(intent);

                                    }
                                }


                                @Override
                                public void onFailure(Call <UserResponse> call, Throwable t) {
                                    Toast.makeText(phonecode.this, t.getMessage(), Toast.LENGTH_LONG).show();
                                    Log.d("login fail" , t.getMessage());


                                }
                            });


                    //    Intent intent = new Intent(phonecode.this,signup.class);
                    //   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    // startActivity(intent);
                }//else


                //{Toast.makeText(phonecode.this,task.getException().getMessage(),Toast.LENGTH_LONG).show();}


            }
        })

        ;
    }

    private  void  sendvarificationcode(String number){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(number, 30, TimeUnit.SECONDS, TaskExecutors.MAIN_THREAD, mcallback);
    }
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mcallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            varificationid = s;

        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if(code != null){
                varifycode(code);
            }

        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(phonecode.this, e.getMessage(), Toast.LENGTH_LONG).show();

        }


    };
}
