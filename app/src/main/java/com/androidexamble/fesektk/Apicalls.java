package com.androidexamble.fesektk;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Apicalls {
    @POST("/login")
    Call <UserResponse> getLoginUser(@Query("phone") String number);

    @POST("/signup")
    Call <UserResponse> getSignUser(@Body User user);


    Call <UserResponse> getSignUser(boolean first_name);
    @POST("/history")
    Call<History> getHistoryUser(@Query("registeration_token") String token);
    @GET("/nearby")
    Call<List <Place>> getplaces(@Query("lat") double userLat , @Query("lon") double userLon);
    @POST("/addvehicle")
    Call<Responsecarinfo>getinfo(@Body Responsecar responseCarinfo);
    @GET("/vehicle")
    Call<List<Responsecar>> getdatacarinfo(@Query("registeration_token") String token);
    @POST("/schedule")
    Call<ResponseScheduled> getScheduled(@Query("registeration_token") String token);
    @POST("/defult")
    Call<Responsecar>selectcar(@Query("id") int id);
    @POST("/requestTrip")
    Call<Responsedataoffer>setrequest(@Body Responserequest responserequest);
    @POST("/offerTrip")
    Call<Responsedataoffer>setoffer(@Body Responserequest responserequest);
    @GET("/matchpassengers")
    Call<List<Responsedataoffer>> getdata(@Query("offer_id") int offer_id);
    @POST("/selectpassengers")
    Call<List<Responsedataoffer>>setmatchedrequests(@Body List<Responsedataoffer> responsedataoffers,@Query("offer_id") int offer_id);

    @POST("/sendTokens")
    Call<ResponseBody> sendDeviceToken(@Query("registeration_token") String registeration_token , @Query("notification_token") String notification_token);
@POST("/cancleOffer")
    Call<ResponseScheduled>SETcancel(@Query("offer_id") int offer_id);
@POST("/startTime")
    Call<ResponseScheduled>Setstarttime(@Query("id" )Long trip_id,@Query("start_time") Long time);
    @POST("/endTime")
    Call<ResponseScheduled>Setendtime(@Query("id" )Long trip_id,@Query("end_time") Long time);


}
