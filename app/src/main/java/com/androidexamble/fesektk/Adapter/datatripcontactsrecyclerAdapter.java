package com.androidexamble.fesektk.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidexamble.fesektk.Model.datatripContect;
import com.androidexamble.fesektk.R;

import java.util.ArrayList;
import java.util.List;

public class datatripcontactsrecyclerAdapter extends RecyclerView.Adapter<datatripcontactsrecyclerAdapter.ViewHolder> {
    ArrayList<datatripContect> items;


    public datatripcontactsrecyclerAdapter( List<datatripContect> items) {

        this.items = (ArrayList<datatripContect>) items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_datatrip, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        datatripContect contect = items.get(position);
        viewHolder.Passenger.setText(contect.getPassenger());
        viewHolder.editname .setText(contect.getEditname());
        viewHolder.phone .setText(contect.getPhone());
        viewHolder.editfrom.setText(contect.getEditfrom());
        viewHolder.editto.setText(contect.getEditto());
    }



    @Override
    public int getItemCount() {
        return items.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {



        private TextView Passenger,editname, phone, editfrom, editto;

        ViewHolder(@NonNull View View) {
            super(View);
            Passenger = View.findViewById(R.id.textViewpassenger);
            editname = View.findViewById(R.id.editname);
            phone = View.findViewById(R.id.phone);
            editfrom = View.findViewById(R.id.editfrom);
            editto = View.findViewById(R.id.editto);

        }


    }


}

