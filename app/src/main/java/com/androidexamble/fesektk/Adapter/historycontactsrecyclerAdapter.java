package com.androidexamble.fesektk.Adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidexamble.fesektk.HistoryRequestAndOffer;
import com.androidexamble.fesektk.OfferstripItem;
import com.androidexamble.fesektk.R;

import java.util.List;

public class historycontactsrecyclerAdapter extends RecyclerView.Adapter<historycontactsrecyclerAdapter.ViewHolder> {
   private List<HistoryRequestAndOffer> items;

    public historycontactsrecyclerAdapter(List<HistoryRequestAndOffer> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_content_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {


            for (int i = 0; i < items.size(); i++) {

                viewHolder.itemView.setBackgroundColor(Color.WHITE);
                if(items.get(position) instanceof OfferstripItem) {
                    Log.d("test", "instanceof ScheduleOfferItem");
                    viewHolder.itemView.setBackgroundColor(Color.CYAN);
                }

           viewHolder.address.setText(items.get(i).getAddress());
           viewHolder.destination.setText(items.get(i).getDestination());
           viewHolder.cost.setText(Double.toString(items.get(i).getCost()));

           Long epoch= items.get(i).getEndTime();
           Long epoch1=items.get(i).getStartTime();
           String date = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new java.util.Date(epoch*1000));
           String date1 = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new java.util.Date(epoch1*1000));
           viewHolder.end_time.setText(date);
           viewHolder.start_time.setText(date1);


            }}


    @Override
    public int getItemCount() {
        return items.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {


        private TextView end_time,start_time, cost, address, destination;


     public  ViewHolder(@NonNull View View) {

            super(View);

            end_time = View.findViewById(R.id.endTime);
            start_time = View.findViewById(R.id.startTime);
            cost = View.findViewById(R.id.cost);
            address = View.findViewById(R.id.from);
            destination = View.findViewById(R.id.to);
        }


    }

}
