package com.androidexamble.fesektk;

import com.google.gson.annotations.SerializedName;

public class HistoryRequestAndOffer {
    @SerializedName("start_time")
    private long startTime;

    @SerializedName("address")
    private String address;

    @SerializedName("cost")
    private double cost;

    @SerializedName("isOffer")
    private int isOffer;

    @SerializedName("end_time")
    private long endTime;

    @SerializedName("destination")
    private String destination;

    @SerializedName("id")
    private int id;

    public void setStartTime(long startTime){
        this.startTime = startTime;
    }

    public long getStartTime(){
        return startTime;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public String getAddress(){
        return address;
    }

    public void setCost(double cost){
        this.cost = cost;
    }

    public double getCost(){
        return cost;
    }

    public void setIsOffer(int isOffer){
        this.isOffer = isOffer;
    }

    public int getIsOffer(){
        return isOffer;
    }

    public void setEndTime(long endTime){
        this.endTime = endTime;
    }

    public Long getEndTime(){
        return endTime;
    }

    public void setDestination(String destination){
        this.destination = destination;
    }

    public String getDestination(){
        return destination;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    @Override
    public String toString(){
        return
                "RequesttripsItem{" +
                        "start_time = '" + startTime + '\'' +
                        ",address = '" + address + '\'' +
                        ",cost = '" + cost + '\'' +
                        ",isOffer = '" + isOffer + '\'' +
                        ",end_time = '" + endTime + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }
}
