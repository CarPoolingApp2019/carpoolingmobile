package com.androidexamble.fesektk;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class home extends AppCompatActivity {
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        button=findViewById(R.id.button_history);

    }

        public void openRecyclerview(View view) {
            Intent intent=new Intent(home.this, history_bar.class);
            startActivity(intent);

        }

}
