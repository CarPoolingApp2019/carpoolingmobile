package com.androidexamble.fesektk;

public class Offer{
	private int tripActualTime;
	private String address;
	private int latitude;
	private int Offer_id;
	private Place place;
	private User user;
	private int longitude;
	private Vehicle vehicle;

	public void setTripActualTime(int tripActualTime){
		this.tripActualTime = tripActualTime;
	}

	public int getTripActualTime(){
		return tripActualTime;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setLatitude(int latitude){
		this.latitude = latitude;
	}

	public int getLatitude(){
		return latitude;
	}

	public void setId(int id){
		this.Offer_id = Offer_id;
	}

	public int getId(){
		return Offer_id;
	}

	public void setPlace(Place place){
		this.place = place;
	}

	public Place getPlace(){
		return place;
	}

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	public void setLongitude(int longitude){
		this.longitude = longitude;
	}

	public int getLongitude(){
		return longitude;
	}

	public void setVehicle(Vehicle vehicle){
		this.vehicle = vehicle;
	}

	public Vehicle getVehicle(){
		return vehicle;
	}

	@Override
 	public String toString(){
		return 
			"Offer{" + 
			"trip_actual_time = '" + tripActualTime + '\'' + 
			",address = '" + address + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",id = '" + Offer_id + '\'' +
			",place = '" + place + '\'' + 
			",user = '" + user + '\'' + 
			",longitude = '" + longitude + '\'' + 
			",vehicle = '" + vehicle + '\'' + 
			"}";
		}
}
