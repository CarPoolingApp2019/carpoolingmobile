package com.androidexamble.fesektk;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class History{

	@SerializedName("offerstrip")
	private List<OfferstripItem> offerstrip;

	@SerializedName("requesttrips")
	private List<RequesttripsItem> requesttrips;

	public List<OfferstripItem> getOfferstrip() {
		return offerstrip;
	}

	public void setOfferstrip(List<OfferstripItem> offerstrip) {
		this.offerstrip = offerstrip;
	}

	public List<RequesttripsItem> getRequesttrips() {
		return requesttrips;
	}

	public void setRequesttrips(List<RequesttripsItem> requesttrips) {
		this.requesttrips = requesttrips;
	}

	@Override
	public String toString() {
		return "History{" +
				"offerstrip=" + offerstrip +
				", requesttrips=" + requesttrips +
				'}';
	}
}