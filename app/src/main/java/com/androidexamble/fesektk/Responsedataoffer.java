package com.androidexamble.fesektk;

import com.google.gson.annotations.SerializedName;

public class Responsedataoffer{

	@SerializedName("trip_actual_time")
	private Long tripActualTime;

	@SerializedName("address")
	private String address;

	@SerializedName("trip")
	private ResponseTrip trip;

	@SerializedName("request_stauts")
	private String requestStauts;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("id")
	private int id;

	@SerializedName("place")
	private Place place;

	@SerializedName("user")
	private User user;

	@SerializedName("longitude")
	private double longitude;

	public void setTripActualTime(Long tripActualTime){
		this.tripActualTime = tripActualTime;
	}

	public Long getTripActualTime(){
		return tripActualTime;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setTrip(ResponseTrip trip){
		this.trip = trip;
	}

	public ResponseTrip getTrip(){
		return trip;
	}

	public void setRequestStauts(String requestStauts){
		this.requestStauts = requestStauts;
	}

	public String getRequestStauts(){
		return requestStauts;
	}

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setPlace(Place place){
		this.place = place;
	}

	public Place getPlace(){
		return place;
	}

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	public Responsedataoffer(Long tripActualTime, String address, ResponseTrip trip, String requestStauts, double latitude, int id, Place place, User user, double longitude) {
		this.tripActualTime = tripActualTime;
		this.address = address;
		this.trip = trip;
		this.requestStauts = requestStauts;
		this.latitude = latitude;
		this.id = id;
		this.place = place;
		this.user = user;
		this.longitude = longitude;
	}

	@Override
 	public String toString(){
		return 
			"Responsedataoffer{" + 
			"trip_actual_time = '" + tripActualTime + '\'' + 
			",address = '" + address + '\'' + 
			",trip = '" + trip + '\'' + 
			",request_stauts = '" + requestStauts + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",id = '" + id + '\'' + 
			",place = '" + place + '\'' + 
			",user = '" + user + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}