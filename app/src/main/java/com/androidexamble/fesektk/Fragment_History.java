package com.androidexamble.fesektk;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidexamble.fesektk.Adapter.historycontactsrecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static android.support.constraint.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_History extends Fragment {


    public Fragment_History() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_history_contacts_list, container, false);


        final RecyclerView histoyRecyclerView = view.findViewById(R.id.contact_recycler_view);
      histoyRecyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity()));

        SharedPreferences preferences = this.getContext().getSharedPreferences("MYPREFS", MODE_PRIVATE);
        String token = preferences.getString("user_token", null);


        Apimanger.getApis().getHistoryUser(token).enqueue(new Callback<History>() {
            @Override
            public void onResponse(Call <History> call, Response <History> response) {
                if (response.isSuccessful()){
                    History histories = response.body();
                    List<HistoryRequestAndOffer> historyRequestAndOffers = new ArrayList<HistoryRequestAndOffer>();
                    assert histories != null;
                    if(histories.getOfferstrip() != null)
                        historyRequestAndOffers.addAll(histories.getOfferstrip());
                    if(histories.getRequesttrips() != null)
                        historyRequestAndOffers.addAll(histories.getRequesttrips());

                   historycontactsrecyclerAdapter historycontactsrecyclerAdapter = new historycontactsrecyclerAdapter(historyRequestAndOffers);
                   histoyRecyclerView.setAdapter(historycontactsrecyclerAdapter);
//
//                    Log.d(TAG,histories.toString());
                    Log.d(TAG , "HistoryResponse success :" + response.body());
                }

            }

            @Override
            public void onFailure(Call<History> call, Throwable t) {
                System.out.println("AAAAAAAAAAAAAAAAAAAAAAAA");
                Log.d(TAG,"mmmmm"+t.getMessage());

            }
        });


        return view;}}


