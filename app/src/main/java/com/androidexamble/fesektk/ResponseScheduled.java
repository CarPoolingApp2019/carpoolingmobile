package com.androidexamble.fesektk;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ResponseScheduled{

	@SerializedName("scheduleRequest")
	private List<ScheduleRequestItem> scheduleRequest;

	@SerializedName("scheduleOffer")
	private List<ScheduleOfferItem> scheduleOffer;

	public void setScheduleRequest(List<ScheduleRequestItem> scheduleRequest){
		this.scheduleRequest = scheduleRequest;
	}

	public ResponseScheduled(List<ScheduleRequestItem> scheduleRequest, List<ScheduleOfferItem> scheduleOffer) {
		this.scheduleRequest = scheduleRequest;
		this.scheduleOffer = scheduleOffer;
	}

	public List<ScheduleRequestItem> getScheduleRequest(){
		return scheduleRequest;
	}

	public void setScheduleOffer(List<ScheduleOfferItem> scheduleOffer){
		this.scheduleOffer = scheduleOffer;
	}

	public List<ScheduleOfferItem> getScheduleOffer(){
		return scheduleOffer;
	}

	@Override
 	public String toString(){
		return 
			"ResponseScheduled{" + 
			"scheduleRequest = '" + scheduleRequest + '\'' + 
			",scheduleOffer = '" + scheduleOffer + '\'' + 
			"}";
		}
}