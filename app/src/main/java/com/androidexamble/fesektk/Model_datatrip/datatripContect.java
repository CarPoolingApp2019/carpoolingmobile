package com.androidexamble.fesektk.Model_datatrip;

public class datatripContect {
    String Passenger;
    String editname;
    String phone;
    String editfrom;
    String editto;

    public datatripContect(String passenger, String editname, String phone, String editfrom, String editto) {
        Passenger = passenger;
        this.editname = editname;
        this.phone = phone;
        this.editfrom = editfrom;
        this.editto = editto;
    }

    public String getPassenger() {
        return Passenger;
    }

    public String getEditname() {
        return editname;
    }

    public String getPhone() {
        return phone;
    }

    public String getEditfrom() {
        return editfrom;
    }

    public String getEditto() {
        return editto;
    }

    public void setPassenger(String passenger) {
        Passenger = passenger;
    }

    public void setEditname(String editname) {
        this.editname = editname;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEditfrom(String editfrom) {
        this.editfrom = editfrom;
    }

    public void setEditto(String editto) {
        this.editto = editto;
    }

    @Override
    public String toString() {
        return "datatripContect{" +
                "Passenger='" + Passenger + '\'' +
                ", editname='" + editname + '\'' +
                ", phone='" + phone + '\'' +
                ", editfrom='" + editfrom + '\'' +
                ", editto='" + editto + '\'' +
                '}';
    }
}

