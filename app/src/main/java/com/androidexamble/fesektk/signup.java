package com.androidexamble.fesektk;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidexamble.fesektk.PushNotificationService.MyFirebaseInstanceIdService;
import com.androidexamble.fesektk.PushNotificationService.SendNotificationTokenTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class signup extends AppCompatActivity implements View.OnClickListener {
    private EditText edittextphone, edittextfirstname, edittextlastname,edittextemail;
    private Button buttonSignup;
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;
    private Button btnDisplay;
    String selectedgender;
    User user;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        edittextphone = (EditText) findViewById(R.id.phone);
        edittextfirstname = (EditText) findViewById(R.id.first_name);
        edittextlastname = (EditText) findViewById(R.id.last_name);
        edittextemail= (EditText) findViewById(R.id.email);
        radioSexGroup = findViewById(R.id.radioSex);
        radioSexGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.gendermale) {
                    selectedgender = "male";

                } else {
                    selectedgender = "female";
                }

            }
        });


        buttonSignup = (Button) findViewById(R.id.button);

        buttonSignup.setOnClickListener(this);
        // Intent intent = new Intent(signup.this, signup.class);
    }

    private User RegisterUser() {
        String phone = edittextphone.getText().toString().trim();
        String firstname = edittextfirstname.getText().toString().trim();
        Log.d("kojih",firstname);
        String lastname = edittextlastname.getText().toString().trim();
        Log.d("hjh",lastname);
        String email = edittextemail.getText().toString().trim();
        return new User(firstname,lastname,phone,selectedgender,email);


    }

    public void onClick(View view) {
        if (view == buttonSignup) {
            user = RegisterUser();



            Apimanger.getApis().getSignUser(user).enqueue(new Callback <UserResponse>() {
                @Override
                public void onResponse(Call <UserResponse> call, Response <UserResponse> response) {
                    if (response.isSuccessful()) {
                        SharedPreferences preferences = getSharedPreferences("MYPREFS",MODE_PRIVATE);
                        String token = response.body().getToken().toString();
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("user_token",token);
                        editor.commit();

                        MyFirebaseInstanceIdService myFirebaseInstanceIdService = new MyFirebaseInstanceIdService();
                        String notificationToken = myFirebaseInstanceIdService.getDeviceTokens(getApplicationContext());
//                        Log.d("notificationTokens" , notificationToken);

                        SendNotificationTokenTask sendNotificationTokenTask = new SendNotificationTokenTask();
                        sendNotificationTokenTask.sendToken(token, notificationToken);
                        Log.d("test signup " , user.toString());
                        Intent intent = new Intent(signup.this, BasicActivity.class);

                        startActivity(intent);
                        Toast.makeText(signup.this, "Registered successfully", Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onFailure(Call <UserResponse> call, Throwable t) {
                    Toast.makeText(signup.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }


            });

        }
    }


}