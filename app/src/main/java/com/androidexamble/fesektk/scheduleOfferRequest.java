package com.androidexamble.fesektk;

import com.google.gson.annotations.SerializedName;

public class scheduleOfferRequest {
    public scheduleOfferRequest(long startTime, String address, double cost, int isOffer, long endTime, String destination) {
        this.startTime = startTime;
        this.address = address;
        this.cost = cost;
        this.isOffer = isOffer;
        this.endTime = endTime;
        this.destination = destination;
    }

    public scheduleOfferRequest(long startTime, String address, double cost, int isOffer, long endTime, String destination, int offerId, long id) {
        this.startTime = startTime;
        this.address = address;
        this.cost = cost;
        this.isOffer = isOffer;
        this.endTime = endTime;
        this.destination = destination;
        this.offerId = offerId;
        this.id = id;
    }

    @SerializedName("start_time")
    private long startTime;

    @SerializedName("address")
    private String address;

    @SerializedName("cost")
    private double cost;

    @SerializedName("isOffer")
    private int isOffer;

    @SerializedName("end_time")
    private long endTime;

    @SerializedName("destination")
    private String destination;

    @SerializedName("offer_id")
    private int offerId;

    @SerializedName("id")
    private long id;

    public void setStartTime(int startTime){
        this.startTime = startTime;
    }

    public long getStartTime(){
        return startTime;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public String getAddress(){
        return address;
    }

    public void setCost(double cost){
        this.cost = cost;
    }

    public double getCost(){
        return cost;
    }

    public void setIsOffer(int isOffer){
        this.isOffer = isOffer;
    }

    public int getIsOffer(){
        return isOffer;
    }

    public void setEndTime(long endTime){
        this.endTime = endTime;
    }

    public long getEndTime(){
        return endTime;
    }

    public void setDestination(String destination){
        this.destination = destination;
    }

    public String getDestination(){
        return destination;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ScheduleRequestItem{" +
                "startTime=" + startTime +
                ", address='" + address + '\'' +
                ", cost=" + cost +
                ", isOffer=" + isOffer +
                ", endTime=" + endTime +
                ", destination='" + destination + '\'' +
                ", offerId='" + offerId + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
