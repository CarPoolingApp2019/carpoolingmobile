package com.androidexamble.fesektk;


public class ScheduleOfferItem extends scheduleOfferRequest{
    public ScheduleOfferItem(long startTime, String address, double cost, int isOffer, long endTime, String destination) {
        super(startTime, address, cost, isOffer, endTime, destination);
    }

    public ScheduleOfferItem(long startTime, String address, double cost, int isOffer, long endTime, String destination, int offerId, long id) {
        super(startTime, address, cost, isOffer, endTime, destination, offerId, id);
    }
}