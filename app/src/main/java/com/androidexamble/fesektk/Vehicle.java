package com.androidexamble.fesektk;

public class Vehicle{
	private String number;
	private String licenceDriver;
	private String licenceCar;
	private String name;
	private String modle;
	private int id;
	private User user;

	public void setNumber(String number){
		this.number = number;
	}

	public String getNumber(){
		return number;
	}

	public void setLicenceDriver(String licenceDriver){
		this.licenceDriver = licenceDriver;
	}

	public String getLicenceDriver(){
		return licenceDriver;
	}

	public void setLicenceCar(String licenceCar){
		this.licenceCar = licenceCar;
	}

	public String getLicenceCar(){
		return licenceCar;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setModle(String modle){
		this.modle = modle;
	}

	public String getModle(){
		return modle;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	@Override
 	public String toString(){
		return 
			"Vehicle{" + 
			"number = '" + number + '\'' + 
			",licence_driver = '" + licenceDriver + '\'' + 
			",licence_car = '" + licenceCar + '\'' + 
			",name = '" + name + '\'' + 
			",modle = '" + modle + '\'' + 
			",id = '" + id + '\'' + 
			",user = '" + user + '\'' + 
			"}";
		}
}
