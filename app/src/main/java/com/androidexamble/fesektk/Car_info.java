package com.androidexamble.fesektk;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Car_info extends AppCompatActivity {
    public EditText  name,modle,number,licenceCar, licenceDriver;
    Button button;
    private String TAG="GYGJH";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.car_info);
        name = (EditText) findViewById(R.id.car_name);
        modle = (EditText) findViewById(R.id.car_model);
        number = (EditText) findViewById(R.id.car_number);
        licenceCar = (EditText) findViewById(R.id.car_licence);
        licenceDriver = (EditText) findViewById(R.id.driver_licence);
        button = (Button) findViewById(R.id.carinfo);
    }
    private Responsecar carinfo(){
        SharedPreferences preferences = this.getSharedPreferences("MYPREFS", MODE_PRIVATE);
        String token = preferences.getString("user_token", null);

        String Carname=name.getText().toString().trim();
        String Carmodle=modle.getText().toString().trim();
        String Carnumber=number.getText().toString().trim();
        String Carlicence=licenceCar.getText().toString().trim();
        String Driverlicence= licenceDriver.getText().toString().trim();
        String Registertoken=token;


        return  new Responsecar(Carname,Carmodle,Carnumber,Carlicence,Driverlicence,Registertoken);}




    public void onClick(View view) {
        Apimanger.getApis().getinfo(carinfo()).enqueue(new Callback<Responsecarinfo>() {
            @Override
            public void onResponse(Call<Responsecarinfo> call, Response<Responsecarinfo> response) {

            }

            @Override
            public void onFailure(Call<Responsecarinfo> call, Throwable t) {

            }
        });

    }}

