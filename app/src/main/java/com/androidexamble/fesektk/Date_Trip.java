package com.androidexamble.fesektk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.androidexamble.fesektk.Adapter.datatripcontactsrecyclerAdapter;
import com.androidexamble.fesektk.Model.datatripContect;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Date_Trip extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    TextView from, to;
    private ImageButton btndatatrip, btnpassenger;
    private final float DEFAULT_ZOOM = 15f;

    ViewGroup tocontainer;
    RecyclerView datatripRecyclerView;
    datatripcontactsrecyclerAdapter Adapter;

    LinearLayoutManager layoutManager;
    ArrayList <datatripContect> data;
    private Context context;
    boolean isopened = false;
    boolean isopened1 = false;
    private GoogleMap mMap;
    private boolean isclicked = false;
    List <datatripContect> list1 = new ArrayList <>();
    private static final String TAG = "TAG";
    boolean issechedled;
    Long trip_id;

Button confirm;
Button endride;
    datatripContect datatripContect;


    List <Responsedataoffer> list2 = new ArrayList <>();
    Responsedataoffer responsedataoffer;
 int Offer_id;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date__trip);
        Offer_id = getIntent().getIntExtra("offer_id",0
        );
        endride=findViewById(R.id.buttonendride);
        endride.setVisibility(View.INVISIBLE);
confirm=findViewById(R.id.buttonconfirm);
issechedled=getIntent().getBooleanExtra("issechedled",false);
         if(issechedled==true){
    confirm.setText("Start trip");

    endride=findViewById(R.id.buttonendride);
    endride.setVisibility(View.VISIBLE);



}

//        super.onCreate(savedInstanceState);
      //  setContentView(R.layout.fragment_fragment_map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map1);
        mapFragment.getMapAsync(this);
        datatripRecyclerView = findViewById(R.id.contact_recycler);
        layoutManager = new LinearLayoutManager(this);


        Call<List<Responsedataoffer>> call = Apimanger.getApis().getdata(Offer_id);
        call.enqueue(new Callback <List <Responsedataoffer>>() {
                                                         @Override
                                                         public void onResponse(Call <List <Responsedataoffer>> call, Response <List <Responsedataoffer>> response) {
                                                             Log.d(TAG , "onResponse");
                                                             if (response.isSuccessful()) {
                                                                 List <Responsedataoffer> list = response.body();

                                                                 for (int i = 0; i < list.size(); i++) {
                                                                     Log.d(TAG, "inside match passengers call api");
                                                                     Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(list.get(i).getLatitude(), list.get(i).getLongitude())).title(list.get(i).getAddress()));
                                                                     moveCamera(marker.getPosition(),DEFAULT_ZOOM,marker.getTitle());

                                                                     datatripContect = new datatripContect("passenger  " + i, response.body().get(i).getUser().getFirstName(), response.body().get(i).getUser().getPhone(), response.body().get(i).getAddress(), response.body().get(i).getPlace().getName());
                                                                     responsedataoffer = new Responsedataoffer(response.body().get(i).getTripActualTime(), response.body().get(i).getAddress(), response.body().get(i).getTrip(), response.body().get(i).getRequestStauts(), response.body().get(i).getLatitude(), response.body().get(i).getId(), response.body().get(i).getPlace(), response.body().get(i).getUser(), response.body().get(i).getLongitude());
                                                                     onMarkerClick(marker);


                                                                 }


                                                             }


                                                         }

                                                         @Override
                                                         public void onFailure(Call <List <Responsedataoffer>> call, Throwable t) {
                                                             Toast.makeText(Date_Trip.this, t.getMessage(), Toast.LENGTH_LONG).show();
                                                             Log.d(TAG, "matc passengers faild" + t.getMessage());

                                                         }
                                                     }
        );


        from = (TextView) findViewById(R.id.from);
        to = (TextView) findViewById(R.id.to);
        datatripRecyclerView = findViewById(R.id.contact_recycler);
        btndatatrip = (ImageButton) findViewById(R.id.btndatatrip);
        btnpassenger = (ImageButton) findViewById(R.id.btnpassenger);
        tocontainer = (ViewGroup) findViewById(R.id.containertrip);
        Creatcontactslist();
        layoutManager = new LinearLayoutManager(Date_Trip.this);
        Adapter = new datatripcontactsrecyclerAdapter(data);
        datatripRecyclerView.setAdapter(Adapter);
        datatripRecyclerView.setLayoutManager(layoutManager);
        tocontainer.setVisibility(View.VISIBLE);
        datatripRecyclerView.setVisibility(View.VISIBLE);


        btndatatrip.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (isopened == true) {
                    tocontainer.setVisibility(View.INVISIBLE);
                    tocontainer.setVisibility(View.GONE);
                    isopened = false;
                } else {

                    tocontainer.setVisibility(View.VISIBLE);
                    isopened = true;
                }
            }
        });

        btnpassenger.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (isopened1 == true) {
                    datatripRecyclerView.setVisibility(View.INVISIBLE);
                    datatripRecyclerView.setVisibility(View.GONE);
                    isopened1 = false;

                } else {
                    datatripRecyclerView.setVisibility(View.VISIBLE);
                    isopened1 = true;
                }


            }
        });
    }


    public void Creatcontactslist() {
        data = new ArrayList <>();
        /*for (int i = 0; i < 3; i++) {
            data.add(new datatripContect("pass1", "basma salah", "01017986331",
                    "azhar", "giza"));

        }*/
    }
    @Override
    public void onMapReady(GoogleMap googleMap){
        mMap=googleMap;
        mMap.setOnMarkerClickListener(this);


    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        if(isclicked==true){
            marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

            isclicked=false;
            list1.add(datatripContect);
            list2.add(responsedataoffer);
            datatripcontactsrecyclerAdapter datatripcontactsrecyclerAdapter=new datatripcontactsrecyclerAdapter(list1);
            datatripRecyclerView.setAdapter(datatripcontactsrecyclerAdapter);

        }else {marker.setIcon(BitmapDescriptorFactory.defaultMarker());
            list1.remove(datatripContect);
            list2.remove(responsedataoffer);
            datatripcontactsrecyclerAdapter datatripcontactsrecyclerAdapter=new datatripcontactsrecyclerAdapter(list1);
            datatripRecyclerView.setAdapter(datatripcontactsrecyclerAdapter);



            isclicked=true;

        }



        return true;
    }
    public void setRequsts(){
        Apimanger.getApis().setmatchedrequests(list2,Offer_id).enqueue(new Callback <List <Responsedataoffer>>() {
            @Override
            public void onResponse(Call <List <Responsedataoffer>> call, Response <List <Responsedataoffer>> response) {
                if(response.isSuccessful()){
                Log.d(TAG,"REqust is succeful"); }}


            @Override
            public void onFailure(Call <List <Responsedataoffer>> call, Throwable t) {
             Log.d(TAG,t.getMessage());

            }
        });

    }

    private void addMarker(LatLng latLng ,String title) {
        MarkerOptions options = new MarkerOptions().position(latLng).title(title);
        mMap.addMarker(options);
        Log.d(TAG, "options marker" + options.getTitle());
    }

    public void onclickdata(View view) {
        if(issechedled==false){
        setRequsts();
        }else if (issechedled==true){
        Long tsLongstart = System.currentTimeMillis()/1000;
        trip_id=getIntent().getLongExtra("trip_id",0);
        Apimanger.getApis().Setstarttime(trip_id,tsLongstart).enqueue(new Callback<ResponseScheduled>() {
            @Override
            public void onResponse(Call<ResponseScheduled> call, Response<ResponseScheduled> response) {

            }

            @Override
            public void onFailure(Call<ResponseScheduled> call, Throwable t) {

            }
        });
    }

    }
    private void moveCamera(LatLng latlng ,float zoom , String title){
        Log.d(TAG , "moving the camera to lattitude : " + latlng.latitude +"and long : " + latlng.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng , zoom));

    }

    public void onclickendtrip(View view) {
        Long tsLongend = System.currentTimeMillis()/1000;
        Log.d(TAG,tsLongend.toString());
        Apimanger.getApis().Setendtime(trip_id,tsLongend).enqueue(new Callback<ResponseScheduled>() {
            @Override
            public void onResponse(Call<ResponseScheduled> call, Response<ResponseScheduled> response) {

            }

            @Override
            public void onFailure(Call<ResponseScheduled> call, Throwable t) {

            }
        });

    }
}
