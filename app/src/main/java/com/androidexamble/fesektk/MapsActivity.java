package com.androidexamble.fesektk;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.PendingResult;
import com.google.maps.model.DirectionsResult;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback , GoogleMap.OnInfoWindowClickListener , OnMarkerClickListener, DatePickerDialog.OnDateSetListener {
    public static final int ERROR_DIALOG_REQUEST = 9001;
    public static final int PERMISSIONS_REQUEST_ENABLE_GPS = 9002;
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 9003;
    public static final String TAG = "Login";
    private GoogleMap mMap;
    private boolean mLocationPermissionGranted = false;
    private FusedLocationProviderClient mFusedLocationClient;
    private final float DEFAULT_ZOOM = 15f;
    private EditText mSearchText;
    private GeoApiContext mGeoApiContext = null;
    Location mUserposition;
    private EditText distinationSerch;
    public double lat;
    public double lon;
    public double sourceLat;
    public double sourceLon;
    public String sourceTitle;
    Button confirm;
    Location destination;
    Location source;
    List<Place> nearDestinationPlaces;
    int id;

  String destinationtitle;
  private EditText starttime;

private  EditText date;

  int mhour;
  int mmiunit;
  Long Time2;

    public MapsActivity() throws ParseException {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mSearchText = (EditText) findViewById(R.id.input_search);
        distinationSerch = (EditText) findViewById(R.id.search_distination);
        confirm = findViewById(R.id.RequestBtn);
        starttime=findViewById(R.id.starttime2);

        date=findViewById(R.id.date);






        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        getLocationPermission();

        init(mSearchText);
        init(distinationSerch);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    private  void init(final EditText inputSearch){

        Log.d(TAG , "init : initialization");
        inputSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH
                        ||actionId == EditorInfo.IME_ACTION_DONE
                        ||event.getAction() == event.ACTION_DOWN
                        ||event.getAction() == event.KEYCODE_ENTER){

                    geolocate(inputSearch);
                }
                return  false;
            };
        });
    }





    private void geolocate(EditText inputSearch){
        Log.d(TAG , "geolocate : geolocating");
        String searchString = inputSearch.getText().toString();
        Geocoder geocoder = new Geocoder(MapsActivity.this);
        List<Address> list = new ArrayList<>();
        try{
            list = geocoder.getFromLocationName(searchString , 1);
        }catch(IOException e){
            Log.e(TAG ,"geoloacate : IOException" + e.getMessage());
        }
        if(list.size() >0){
            Address address = list.get(0);
            Log.d(TAG , "geolocate : found a location" +address.toString());
            lat = address.getLatitude();
            lon = address.getLongitude();

            if(inputSearch ==mSearchText){
                moveCamera(new LatLng(address.getLatitude() , address.getLongitude()) , DEFAULT_ZOOM , address.getAddressLine(0));
                sourceLat = address.getLatitude();
                sourceLon = address.getLongitude();
                sourceTitle = address.getAddressLine(0);
                Log.d(TAG , "sourceLat" + sourceLat +"sourcLon" +sourceLon);
            }else {
                getNearbyPlaces(Math.toRadians(lat), Math.toRadians(lon));
                moveCamera(new LatLng(address.getLatitude(), address.getLongitude()), DEFAULT_ZOOM, address.getAddressLine(0));
            }
        }
    }
    private  void  checkGetNearbyPlacesResponse(List<Place> places , double lat , double lon){
        if(places.size() ==  1&& places.get(0).getLat() ==lat && places.get(0).getLon() ==lon){
            Log.d(TAG , "you enter place that exist in db");
            moveCamera(new LatLng(places.get(0).getLat(), places.get(0).getLon()) , DEFAULT_ZOOM , places.get(0).getName());
        }else {
            mMap.clear();
            for(Place place : places){

                addMarker(new LatLng(Math.toDegrees(place.getLat()) , Math.toDegrees(place.getLon())) , place.getName());

                Log.d(TAG , "place >>" +place.getName());

            }
        }

    }



    private void getNearbyPlaces(final double lat , final double lon){
        Log.d(TAG , " getNearByPlaces");
        Call <List<Place>> call = Apimanger.getApis().getplaces(lat , lon);
        call.enqueue(new Callback <List<Place>>() {
            @Override
            public void onResponse(Call<List<Place>> call, Response <List<Place>> response) {
                if(response.isSuccessful()){

                    List<Place> places = response.body();
                    nearDestinationPlaces = places;
                    checkGetNearbyPlacesResponse(places , lat , lon);


                }
            }

            @Override
            public void onFailure(Call<List<Place>> call, Throwable t) {

                Log.d(TAG , "getNearbyPlaces fail cause :" + t.getMessage());
            }
        });

    }






    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerClickListener(this);



        if (mLocationPermissionGranted) {
            getDeviceLocation();

        }


    }

    private void moveCamera(LatLng latlng ,float zoom , String title){
        Log.d(TAG , "moving the camera to lattitude : " + latlng.latitude +"and long : " + latlng.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng , zoom));
        addMarker(latlng , title);
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("This application requires GPS to work properly, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Intent enableGpsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(enableGpsIntent, PERMISSIONS_REQUEST_ENABLE_GPS);
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public boolean isMapsEnabled() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
            return false;
        }
        return true;
    }

    private void initMap() {
        Log.d(TAG, "initMAp : intializing Map");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        if (mGeoApiContext == null) {
            mGeoApiContext = new GeoApiContext.Builder()
                    .apiKey("AIzaSyD2gggSDFPb_jZkJR1fOL3AnGL8IO2ixf0")
                    .build();
        }




    }

    private void getLocationPermission() {

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            initMap();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    public boolean isServicesOK() {
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: called.");
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ENABLE_GPS: {
                if (mLocationPermissionGranted) {
                    initMap();
                } else {
                    getLocationPermission();
                }
            }
        }

    }


    private boolean checkMapServices() {
        if (isServicesOK()) {
            if (isMapsEnabled()) {
                return true;
            }
        }
        return false;
    }


    private void getDeviceLocation() {

        Log.d(TAG, "getDeviceLocation called");
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            if (mLocationPermissionGranted) {
                final Task location = mFusedLocationClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {

                        Log.d(TAG, "onComplete : Location founded");

                        Location currentLocation = (Location) task.getResult();
                        if (task.isSuccessful() && currentLocation != null) {



                            mUserposition = currentLocation;
                            moveCamera(new LatLng(mUserposition.getLatitude(),mUserposition.getLongitude()),15f,"current location");


                        } else {
                            Log.d(TAG, "current location is null");
                            Toast.makeText(MapsActivity.this, "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.d(TAG, "getDeviceLocation : SecurityException" + e.getMessage());
        }
    }
    private void addMarker(LatLng latLng ,String title){
        MarkerOptions options =new MarkerOptions().position(latLng).title(title);
        mMap.addMarker(options);
        Log.d(TAG , "options marker" +options.getTitle());
    }


    public boolean onMarkerClick(Marker marker) {

        double lat = marker.getPosition().latitude;
        double lon = marker.getPosition().longitude;
        for (Place nearPlace : nearDestinationPlaces){
            Log.d(TAG,"nearDestinationPlaces" + nearDestinationPlaces.get(0).getName());
            Log.d(TAG , "inside on marker click in for loop");
            if(nearPlace.getLat() ==Math.toRadians(lat)&&nearPlace.getLon()==Math.toRadians(lon)){
                id =nearPlace.getId();
                Log.d(TAG , "nearPlace ID " + id);

            }
        }

        destinationtitle =marker.getTitle();
        mMap.clear();
        addMarker(new LatLng(sourceLat , sourceLon) , sourceTitle);

        addMarker(new LatLng(lat , lon) , marker.getTitle());





        Log.d(TAG , "distination lat :" + lat);
        Log.d(TAG , "distination lon :" + lon);



        return false;

    }




    @Override
    public void onInfoWindowClick(final Marker marker) {
        Log.d(TAG, "onfow");
        if (marker.getTitle().equals(sourceTitle)) {
            marker.hideInfoWindow();
        } else {

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            Log.d(TAG, "Alert");
            builder.setMessage(marker.getTitle())

                    .setCancelable(true)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.dismiss();
                            calculateDirections(marker);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void calculateDirections(Marker marker) {
        Log.d(TAG, "calculateDirections: calculating directions.");

        com.google.maps.model.LatLng destination = new com.google.maps.model.LatLng(
                marker.getPosition().latitude,
                marker.getPosition().longitude
        );
        DirectionsApiRequest directions = new DirectionsApiRequest(mGeoApiContext);

        directions.alternatives(true);
        directions.origin(
                new com.google.maps.model.LatLng(
                        mUserposition.getLatitude(),
                        mUserposition.getLongitude()
                )
        );
        Log.d(TAG, "calculateDirections: destination: " + destination.toString());
        directions.destination(destination).setCallback(new PendingResult.Callback <DirectionsResult>() {

            public void onResult(DirectionsResult result) {
                result.routes.clone();
                result.geocodedWaypoints.clone();


                Log.d(TAG, "onResult: routes: " + result.routes[0].toString());
                Log.d(TAG, "onResult: geocodedWayPoints: " + result.geocodedWaypoints[0].toString());

                Log.d(TAG,"calculateDirections :duration" + result.routes[0].legs[0].duration);
                Log.d(TAG,"calculateDirections :distance" + result.routes[0].legs[0].distance);
            }


            public void onFailure(Throwable e) {
                Log.e(TAG, "onFailure: " + e.getMessage());

            }
        });


    }





    public void onclicconfirm(View view) throws ParseException {
        boolean isoffer = getIntent().getBooleanExtra("isoffer", true);
        Log.d(TAG,"is offer is "+ isoffer);

        SharedPreferences preferences = getSharedPreferences("MYPREFS", MODE_PRIVATE);
        String token = preferences.getString("user_token", null);
        Log.d("test_pref", "test pref: "+token);

        Responserequest responserequest=new Responserequest(Time2,sourceTitle,token,sourceLat,id,sourceLon);

        if (isoffer==false) {
            Log.d(TAG,"inside request trip api");
            Apimanger.getApis().setrequest(responserequest).enqueue(new Callback <Responsedataoffer>() {
                @Override
                public void onResponse(Call <Responsedataoffer> call, Response <Responsedataoffer> response) {
                    if(response.isSuccessful()){
                    Log.d(TAG, "Requst is succful"+id);}
                 //   Log.d(TAG,responserequest.toString());
Log.d(TAG,"time stamp is"+  String.valueOf(Time2));

                }

                @Override
                public void onFailure(Call <Responsedataoffer> call, Throwable t) {
                    Toast.makeText(MapsActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });


        }else if (isoffer==true) {
            Log.d(TAG,"inside offer trip");
            Apimanger.getApis().setoffer(responserequest).enqueue(new Callback <Responsedataoffer>() {

                @Override
                public void onResponse(Call <Responsedataoffer> call, Response <Responsedataoffer> response) {
                    if(response.isSuccessful())
                    {
                        Log.d(TAG,"Offer is succful"+response.body().getId());
                        Intent intent = new Intent(MapsActivity.this,Date_Trip.class);
                        intent.putExtra("offer_id",response.body().getId());
                        startActivity(intent);
                    }

                }

                @Override
                public void onFailure(Call <Responsedataoffer> call, Throwable t) {
                    Log.d(TAG,"kkkkkkkkkkkkkkkkkkkkkkkkk");

                }
            });




        }
    }


    public void onClicktime(View view) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MapsActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                starttime.setText( selectedHour + ":" + selectedMinute);
    }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }

    @Override
    public void onDateSet(DatePicker view, int year1, int month1, int day1) {

        date.setText(day1 + "/" + (month1+1) + "/" + year1);
        Calendar calendar = new GregorianCalendar(year1, month1, day1,mhour,mmiunit);
      Long Time1= calendar.getTimeInMillis();
      Time2=Time1;
        view.setMinDate(System.currentTimeMillis() - 1000);



    }

    public void onClickDate(View view) {
        DatePickerDialog dialog = new DatePickerDialog(this, this, 2013, 2, 18);

        dialog.show();



    }



}






