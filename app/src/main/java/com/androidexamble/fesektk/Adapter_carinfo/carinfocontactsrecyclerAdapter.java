package com.androidexamble.fesektk.Adapter_carinfo;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidexamble.fesektk.R;
import com.androidexamble.fesektk.Responsecar;

import java.util.ArrayList;
import java.util.List;


public class carinfocontactsrecyclerAdapter extends RecyclerView.Adapter<carinfocontactsrecyclerAdapter.ViewHolder> {
    ArrayList<Responsecar> items;
    private Context context;

    public carinfocontactsrecyclerAdapter(List<Responsecar> items) {
        this.items = (ArrayList<Responsecar>) items;
    }



    @NonNull
    @Override
    public carinfocontactsrecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_carinfo, parent, false);
        return new com.androidexamble.fesektk.Adapter_carinfo.carinfocontactsrecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull carinfocontactsrecyclerAdapter.ViewHolder viewHolder, int position) {
        Responsecar contect = items.get(position);
        viewHolder.name.setText(contect.getName());
        viewHolder.number .setText(contect.getNumber());

    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {


        private TextView name, number;

        ViewHolder(@NonNull View View) {
            super(View);
            name = View.findViewById(R.id.editcarname);
            number = View.findViewById(R.id.editcarnumber);

        }


    }


}

