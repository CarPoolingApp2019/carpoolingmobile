package com.androidexamble.fesektk;

import com.google.gson.annotations.SerializedName;


public class UserResponse {

    @SerializedName("user")
    private User user;

    @SerializedName("token")
    private String token;

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return
                "UserResponse{" +
                        "user = '" + user + '\'' +
                        ",token = '" + token + '\'' +
                        "}";
    }
}
