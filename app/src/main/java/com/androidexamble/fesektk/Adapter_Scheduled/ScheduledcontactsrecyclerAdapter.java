package com.androidexamble.fesektk.Adapter_Scheduled;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.androidexamble.fesektk.Apimanger;
import com.androidexamble.fesektk.Date_Trip;
import com.androidexamble.fesektk.R;
import com.androidexamble.fesektk.ResponseScheduled;
import com.androidexamble.fesektk.ScheduleOfferItem;
import com.androidexamble.fesektk.scheduleOfferRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScheduledcontactsrecyclerAdapter extends RecyclerView.Adapter<ScheduledcontactsrecyclerAdapter.ViewHolder> {
    List<scheduleOfferRequest> items;
    int isOffer = 0;
    private Context context;

    public ScheduledcontactsrecyclerAdapter(Context context, List<scheduleOfferRequest> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public ScheduledcontactsrecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_scheduled, parent, false);
        return new com.androidexamble.fesektk.Adapter_Scheduled.ScheduledcontactsrecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduledcontactsrecyclerAdapter.ViewHolder viewHolder, final int position) {



                viewHolder.itemView.setBackgroundColor(Color.WHITE);
                viewHolder.isOffer.setText("request");
                if(items.get(position) instanceof ScheduleOfferItem) {
                    Log.d("test", "instanceof ScheduleOfferItem");
                    viewHolder.itemView.setBackgroundColor(Color.CYAN);
                    viewHolder.isOffer.setText("offer");

                    viewHolder.cancel.setVisibility(View.VISIBLE);
                    viewHolder.cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d("test", "request"+items.get(position).getOfferId());
                            //TODO call cancel offer
                            Apimanger.getApis().SETcancel(items.get(position).getOfferId()).enqueue(new Callback<ResponseScheduled>() {
                                @Override
                                public void onResponse(Call<ResponseScheduled> call, Response<ResponseScheduled> response) {
                                    if(response.isSuccessful()){
                                        Log.d("TAG","kjdfjklejfejklfwejfwjk");
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseScheduled> call, Throwable t) {

                                }
                            });

                        }

                    });
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(context, Date_Trip.class);
                            intent.putExtra("issechedled",true);
                            intent.putExtra("trip_id",items.get(position).getId());
                           context.startActivity(intent);



                        }
                    });
                }
                viewHolder.start_time.setText(Long.toString(items.get(position).getStartTime()));
                viewHolder.End_time.setText(Long.toString(items.get(position).getEndTime()));
                viewHolder.address.setText(items.get(position).getAddress());
                viewHolder.destination.setText(items.get(position).getDestination());






    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {


        private TextView start_time, address, isOffer, destination, End_time;
        private Button cancel;


        ViewHolder(@NonNull View viewInstance) {

            super(viewInstance);
            start_time = viewInstance.findViewById(R.id.start_time);
            address = viewInstance.findViewById(R.id.address);
            isOffer = viewInstance.findViewById(R.id.offer);
            destination = viewInstance.findViewById(R.id.destination);
            End_time = viewInstance.findViewById(R.id.endTime);
            cancel = viewInstance.findViewById(R.id.cancel);
        }


    }




}



