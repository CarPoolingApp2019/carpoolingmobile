package com.androidexamble.fesektk;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {
	private String gender;
	private String phone;
	@SerializedName("second_name")
	private String secondName;
	@SerializedName("first_name")
	private String firstName;
	private String email;
	private Long id;

	public User(String firstName, String secondName, String phone, String gender, String email) {
		this.gender = gender;
		this.phone = phone;
		this.secondName = secondName;
		this.firstName = firstName;
		this.email = email;
	}

	public User(String gender, String phone, String secondName, String firstName, String email, Long id) {
		this.gender = gender;
		this.phone = phone;
		this.secondName = secondName;
		this.firstName = firstName;
		this.email = email;
		this.id = id;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGender() {
		return gender;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "User{" +
				"gender='" + gender + '\'' +
				", phone='" + phone + '\'' +
				", secondName='" + secondName + '\'' +
				", firstName='" + firstName + '\'' +
				", email='" + email + '\'' +
				", id=" + id +
				'}';
	}
}