package com.androidexamble.fesektk;

import com.google.gson.annotations.SerializedName;


public class Responserequest{

	@SerializedName("trip_actual_time")
	private long tripActualTime;

	@SerializedName("address")
	private String address;

	@SerializedName("registeration_token")
	private String registerationToken;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("destination_place_id")
	private int destinationPlaceId;

	@SerializedName("longitude")
	private double longitude;

	public void setTripActualTime(long tripActualTime){
		this.tripActualTime = tripActualTime;
	}

	public long getTripActualTime(){
		return tripActualTime;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setRegisterationToken(String registerationToken){
		this.registerationToken = registerationToken;
	}

	public String getRegisterationToken(){
		return registerationToken;
	}

	public void setLatitude(int latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setDestinationPlaceId(int destinationPlaceId){
		this.destinationPlaceId = destinationPlaceId;
	}

	public int getDestinationPlaceId(){
		return destinationPlaceId;
	}

	public void setLongitude(int longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	public Responserequest(long tripActualTime, String address, String registerationToken, double latitude, int destinationPlaceId, double longitude) {
		this.tripActualTime = tripActualTime;
		this.address = address;
		this.registerationToken = registerationToken;
		this.latitude = latitude;
		this.destinationPlaceId = destinationPlaceId;
		this.longitude = longitude;
	}

	@Override
 	public String toString(){
		return 
			"Responserequest{" + 
			"trip_actual_time = '" + tripActualTime + '\'' + 
			",address = '" + address + '\'' + 
			",registeration_token = '" + registerationToken + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",destination_place_id = '" + destinationPlaceId + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}