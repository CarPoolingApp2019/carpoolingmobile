package com.androidexamble.fesektk.PushNotificationService;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseInstanceIdService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String Token ) {
        Log.e("NEW_TOKEN", Token);
        super.onNewToken(Token);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.d("test11", "onSuccess: " + newToken);

                SharedPreferences preferences ;
                preferences =getSharedPreferences("MYPREFS",MODE_PRIVATE);
                
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("device_notification_token",newToken);
                editor.apply();


            }
        });




    }
    public String getDeviceTokens(final Context context){

        SharedPreferences preferences = context.getSharedPreferences("MYPREFS", MODE_PRIVATE);

        String notification_token = preferences.getString("device_notification_token", null);
        Log.d("DeviceToken",notification_token);

//        SharedPreferences preferences ;
//        preferences =context.getSharedPreferences("MYPREFS",MODE_PRIVATE);
//
//        String notification_token =  preferences.getString("device_notification_token" ,null);
        return notification_token;
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage){
        super.onMessageReceived(remoteMessage);
        if(remoteMessage.getData() != null){
           sendNotification(remoteMessage);
        }

    }

    private void sendNotification(RemoteMessage remoteMessage) {
        Map<String , String> date = remoteMessage.getData();
        String title = date.get("title");
        String content = date.get("content");
        NotificationManager notificationManager =(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "EDMTDev";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            //active for android o and higher because its need Notification channel
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID , "EDMT Notification" ,NotificationManager.IMPORTANCE_DEFAULT);

            //configure notification channel
            notificationChannel.setDescription("EDMTDev channel for carpooling App");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long []{0,1000,500,1000});
            notificationChannel.enableVibration(true);

            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this ,NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis()).setSmallIcon(android.support.v4.R.drawable.notification_icon_background)
                .setTicker("Hearty365")
                .setContentTitle(title)
                .setContentText(content)
                .setContentInfo("info");

        notificationManager.notify(1 , notificationBuilder.build());



    }

}
