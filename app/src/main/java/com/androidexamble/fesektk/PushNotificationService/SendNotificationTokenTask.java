package com.androidexamble.fesektk.PushNotificationService;

import android.util.Log;

import com.androidexamble.fesektk.Apimanger;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendNotificationTokenTask {

    public void sendToken(String registeration_token, String notification_token){

        Apimanger.getApis().sendDeviceToken(registeration_token ,notification_token).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    Log.d("sendDeviceToken" ,"success");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("send tokens fail" , t.getMessage());

            }
        });
    }
}
