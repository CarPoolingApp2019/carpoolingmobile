package com.androidexamble.fesektk;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;


public class splash extends AppCompatActivity{





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
setContentView(R.layout.splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedPreferences preferences = getSharedPreferences("MYPREFS", MODE_PRIVATE);
                String token =preferences.getString("user_token",null);
                if(token==null){

                    Intent intent = new Intent(splash.this,login.class);
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(splash.this,BasicActivity.class);
                    startActivity(intent);

                }
            }
        }, 3500);


    }
}
