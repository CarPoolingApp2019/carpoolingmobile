package com.androidexamble.fesektk;

public class ResponseTrip{
	private Offer offer;
	private int startTime;
	private int cost;
	private int endTime;
	private int id;

	public void setOffer(Offer offer){
		this.offer = offer;
	}

	public Offer getOffer(){
		return offer;
	}

	public void setStartTime(int startTime){
		this.startTime = startTime;
	}

	public int getStartTime(){
		return startTime;
	}

	public void setCost(int cost){
		this.cost = cost;
	}

	public int getCost(){
		return cost;
	}

	public void setEndTime(int endTime){
		this.endTime = endTime;
	}

	public int getEndTime(){
		return endTime;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"ResponseTrip{" + 
			"offer = '" + offer + '\'' + 
			",start_time = '" + startTime + '\'' + 
			",cost = '" + cost + '\'' + 
			",end_time = '" + endTime + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}
