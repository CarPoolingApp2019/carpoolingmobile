package com.androidexamble.fesektk;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Responsecar implements Serializable {
	@SerializedName("number")
	private String number;
	@SerializedName("licence_driver")
	private String licenceDriver;
	@SerializedName("registeration_token")
	private String registerationToken;
	@SerializedName("licence_car")
	private String licenceCar;
	@SerializedName("name")
	private String name;
	@SerializedName("modle")
	private String modle;

	public Responsecar(String name,String modle,String number,String licence_Car,String licence_Driver,String registeration_Token) {
		this.number = number;
		this.licenceDriver = licence_Driver;
		this.registerationToken = registeration_Token;
		this.licenceCar = licence_Car;
		this.name = name;
		this.modle = modle;
	}

	public void setNumber(String number){
		this.number = number;
	}

	public String getNumber(){
		return number;
	}

	public void setLicenceDriver(String licenceDriver){
		this.licenceDriver = licenceDriver;
	}

	public String getLicenceDriver(){
		return licenceDriver;
	}

	public void setRegisterationToken(String registerationToken){
		this.registerationToken = registerationToken;
	}

	public String getRegisterationToken(){
		return registerationToken;
	}

	public void setLicenceCar(String licenceCar){
		this.licenceCar = licenceCar;
	}

	public String getLicenceCar(){
		return licenceCar;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setModle(String modle){
		this.modle = modle;
	}

	public String getModle(){
		return modle;
	}

	@Override
 	public String toString(){
		return 
			"Responsecar{" + 
			"number = '" + number + '\'' + 
			",licence_driver = '" + licenceDriver + '\'' + 
			",registeration_token = '" + registerationToken + '\'' + 
			",licence_car = '" + licenceCar + '\'' + 
			",name = '" + name + '\'' + 
			",modle = '" + modle + '\'' + 
			"}";
		}
}
